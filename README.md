# Music-spider 橡皮音乐

### Introduction
音乐播放器/下载器，五大歌曲库：酷狗 + QQ + 百度 + 虾米 + 网易，合称中华曲库

### Feature
 - HTML5 Player
 - Lyric Display
 - 400px Album Display(max)
 - Hot Search Display
 - Lyric Spacing Auto Adjust
 - Music Download
 - Music Share
 - Responsive Design
 - Windows && Linux Support

### Essential Code Quote
1. Meting.php by https://github.com/metowolf/Meting

<p align="center">
<img src="https://images.gitee.com/uploads/images/2019/0724/144303_ea95e290_1785190.png" alt="Meting">
</p>

2. APlayer.min.js https://github.com/MoePlayer/APlayer

<p align="center">
<img src="https://gitee.com/lifanko/others/raw/master/images/aplayer.jpg" alt="ADPlayer" width="100">
</p><h1 align="center">APlayer</h1>

### Android App
 + v1.0.0 2019/03/27 http://A6051308906702.qiniucdn.apicloud-system.com/79c5347e472e43227900ffea022cf536_d

### Demo
https://www.lifanko.cn/music/

<h3 align="center">Desktop</h1>
<p align="center">
<img src="https://gitee.com/lifanko/others/raw/master/images/eraser.png" alt="ADPlayer">
</p>

<h3 align="center">App</h1>
<p align="center">
<img src="https://gitee.com/lifanko/others/raw/master/images/eraser-app.png" alt="ADPlayer">
</p>

### Extra
 + The TXT files should possess write permissions.
 + If you'd like to using these code on your website, please contact lifankohome@163.com and get AUTHORIZATION first.

### How To Install Correctly :tw-1f6a9:  :tw-1f6a9:  :tw-1f6a9: 
我发现很多小伙伴的网站打开后没有默认列表，，，，，有的甚至没有热搜词，，，，，

如果出现了这样的情况，你需要在根目录新建两个文件，一个是playList.txt，一个是hotSearch.txt，两个文件都必须可写！！！

- playList.txt用于保存默认列表（用户搜索量最多的一首歌）
- hotSearch.txt用于保存搜索关键词（用户搜索过的关键词）

Meting.php进行了一点修改，所以在执行composer install之后你需要将下载的Meting.php替换（问：既然这样为什么还要使用composer？答：因为自动加载更方便呀~）

APlayer.min.js也进行了一点修改，建议下载后放到自己的cdn或者服务器目录，本宝宝的cdn流量不多呀……

如果老板捐赠了，上句话当我没说，请尽情使用宝宝的cdn :tw-1f606: 
